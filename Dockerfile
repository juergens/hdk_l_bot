FROM ubuntu:19.04
LABEL cache_breaker=2019_03_29

ENV PYTHONUNBUFFERED 0

### general python deps
RUN apt-get update && apt-get install -y \
	python3-pip \
	python3-dev \
	python3

### general python setup
WORKDIR /bot
COPY requirements.txt /bot
RUN pip3 install -r requirements.txt
COPY hdk_l_bot/ /bot/src/
COPY hdk_l_bot/migrations/ /bot/src/migrations/
WORKDIR /bot/src/
CMD ["/bot/src/__init__.py"]
