import email
from email import policy
import imaplib
import time

import html2text

from abstract_update import abstract_update
from helper import log_error


class MailException(Exception):
    pass


class Handler(object):

    def __init__(self, imap_server, imap_user, imap_pass, callback, debug_stop_after_first=False,
                 debug_use_dummy_mails=False):
        self.callback = callback
        self.imap_server = imap_server
        self.imap_user = imap_user
        self.imap_pass = imap_pass
        self.debug_stop_after_first = debug_stop_after_first
        self.debug_use_dummy_mails = debug_use_dummy_mails

    def __call__(self):
        self.polling_and_idling()

    def polling_and_idling(self):
        while True:
            try:
                mail = imaplib.IMAP4_SSL(self.imap_server)
                (retcode, capabilities) = mail.login(self.imap_user, self.imap_pass)
                if retcode != 'OK':
                    raise MailException("can't login to email")
                while True:
                    mail.list()
                    mail.select('inbox')

                    if self.debug_use_dummy_mails:
                        (retcode, messages) = mail.search(None, 'SUBJECt', 'Fahrradbremshebel')
                    else:
                        (retcode, messages) = mail.search(None, '(UNSEEN)')

                    if retcode != 'OK':
                        raise MailException("can't search new emails")

                    for num in messages[0].split():
                        print('Processing ')
                        (typ1, data) = mail.fetch(num, '(RFC822)')

                        msg = email.message_from_bytes(data[0][1], policy=policy.EmailPolicy())
                        _subject = email.header.make_header(email.header.decode_header(msg['Subject']))
                        _from = email.header.make_header(email.header.decode_header(msg['From']))

                        body = None
                        for payload in msg.walk():
                            # prefer text. If no text, use html
                            if payload.get_content_type().lower() == 'text/plain':
                                body = payload.get_content()
                                break
                            if payload.get_content_type().lower() == 'text/html':
                                # html fickt mit der kodierung und mit den zeilenumbrüchen
                                body = html2text.HTML2Text().handle(payload.get_content())
                        if body is None:
                            body = "<Diese Nachricht hat keinen Inhalt>"

                        # there may only be max one consecutive blanc line
                        # for some reason some emails are shit. I think this is because they get created
                        # with a mix of divs and br and margins which all turn into invisible line breaks.
                        trimmed = []
                        drop_next_empty = False
                        for line in body.splitlines():
                            if drop_next_empty:
                                if len(line.strip()) == 0:
                                    continue
                            drop_next_empty = False
                            if len(line.strip()) == 0:
                                drop_next_empty = True

                            trimmed = trimmed + [line]
                        body = "\n".join(trimmed)

                        self.callback(abstract_update(
                            text=str(body),
                            subject=str(_subject),
                            sender=str(_from)
                        ))
                        if self.debug_stop_after_first:
                            return
                        else:
                            time.sleep(5)

                        # apparently not needed with gmail, but still doit just to be sure.
                        (typ2, data) = mail.store(num, '+FLAGS', '\\Seen')

            except Exception as e:
                # todo: send telegram message to me
                log_error(e)
                time.sleep(10)

    def send(self, chat_id, text):
        raise NotImplementedError("This handler does not yet support sending emails")

    def stop(self):
        raise NotImplementedError("This handler can not be stopped!!!")
