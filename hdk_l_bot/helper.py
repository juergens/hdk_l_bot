import traceback

class PicnicException(Exception):
	"""
	This exception is expected. It's something the user can deal with.
	"""
	pass


def log_error(e):
	if hasattr(e, 'message'):
		msg = e.message
	else:
		msg = e
	tb = traceback.format_exc()
	debug_info = str(e.__class__) + ", " + str(e.__doc__) + ", " + str(msg) + "\n\n " + tb
	l.e(debug_info)


def is_number(s):
	""" Returns True if string is a number. """
	return s.replace('.', '', 1).isdigit()


def s2b(s, default):
	""" Turns string into boolean """
	if not s: return default
	if s == "True": return True
	if s == "False": return False
	raise ValueError("string must be empty, True or False.")


import sys
import logging


class MyLog(object):
	"""
	This is a shortcut for logging
	Usage:
		l("info")
		l.i("info")
		l.w("warning")
		l.e("error")
		l.d("debug")
	"""
	logging.basicConfig(stream=sys.stdout, level=logging.INFO)

	def __call__(self, *args, **kwargs): self.i(*args, **kwargs)

	def i(self, *args, **kwargs): logging.info(*args, **kwargs)

	def e(self, *args, **kwargs): logging.error(*args, **kwargs)

	def w(self, *args, **kwargs): logging.warning(*args, **kwargs)

	def d(self, *args, **kwargs): logging.debug(*args, **kwargs)


l = MyLog()
