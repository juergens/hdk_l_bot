#!/usr/bin/env python3

class abstract_update():
    """
    this class is a defined data-interface for pushing messages between the different chat-interfaces
    """

    def __init__(self, text, chat_id=None, subject=None, sender=None, receiver=None, meta=None):
        self.text = text
        self.subject = subject
        self.sender = sender
        self.receiver = receiver
        self.chat_id = chat_id
