#!/usr/bin/env python3
# deps from core-python
import itertools
import os
from signal import signal, SIGINT, SIGTERM, SIGABRT
from time import sleep

import database_handler
# local import
from helper import *
from imap_handler import Handler as smtp_handler
from telegram_handler import Handler as telegram_handler


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)


class Idler(object):
    """
    this is basically copied from telegram.updated. I don't really know or care what's in here.
    """

    def __init__(self):
        self.logger = logging
        self._sig_names = {v: k
                           for k, v in reversed(sorted(vars(signal).items()))
                           if k.startswith('SIG') and not k.startswith('SIG_')}

    def get_signal_name(self, signum):
        """Returns the signal name of the given signal number."""
        return self._sig_names[signum]

    def signal_handler(self, signum, frame):
        #
        if self.is_idle:
            telegram.stop()
            self.is_idle = False
        else:
            self.logger.warning('Exiting immediately!')
            import os
            os._exit(1)

    def idle(self, stop_signals=(SIGINT, SIGTERM, SIGABRT)):
        for sig in stop_signals:
            signal(sig, self.signal_handler)
        self.running = True
        self.is_idle = True
        while self.is_idle:
            sleep(1)


def get_db():
    return database_handler.Handler(db_file)


def is_telegram_cmd(possible_command, text):
    if text == possible_command:
        return True
    if text == possible_command + telegram_name:
        return True
    return False


def update_from_telegram(abstrac_update):
    logging.info("update from telegram...")
    logging.info(abstrac_update)
    if is_telegram_cmd('/start', abstrac_update.text):
        get_db().add_chat_id(abstrac_update.chat_id)
        telegram.send(abstrac_update.chat_id, "This chat is now subscribed to hadiko-l")
    if is_telegram_cmd('/unsubscribe', abstrac_update.text):
        # get_db().remove_chat_id(abstrac_update.chat_id)
        telegram.send(abstrac_update.chat_id, "This chat is now unsubscribed to hadiko-l")
    if is_telegram_cmd('/help', abstrac_update.text):
        telegram.send(abstrac_update.chat_id, """This bot will bring Hadiko-L to telegram. It forwards emails sent to the mailing lists to telegram-chats. 
        
Commands: 
/start: start receiving messages from hadiko-L
/help: show the help-message""")


def update_from_smtp(abstrac_update):
    logging.info("update from smtp...")
    logging.info(abstrac_update)

    if '[HaDiKo-L]' not in abstrac_update.subject:
        logging.info("ignoring mail, because no [HaDiKo-L] in subject")
        logging.info("subject was: " + abstrac_update.subject)
        if not debug:
            return

    body = (abstrac_update.text[:max_body_length]) if len(
        abstrac_update.text) > max_body_length else abstrac_update.text
    body = "\n".join(itertools.islice(body.splitlines(), max_body_lines))

    if len(body) < len(abstrac_update.text)-1:
        body += "..."

    logging.info("forwarding to telegram...")
    for chat_id in get_db().get_all_chats():
        message = "Subject: " + abstrac_update.subject + "\n" \
                  + "From: " + abstrac_update.sender + "\n\n" \
                  + body
        try:
            telegram.send(chat_id[0], message)
        except Exception as e:
            # todo: send telegram message to me
            log_error(e)


###############
### config vars
###############

max_body_length = 600
max_body_lines = 50

#####################
### start doing stuff
#####################

logging.info("preparing db...")
db_file = "../data/sqlite.db"
database_handler.make_migration(db_file, 'migrations')

logging.info("starting telegram...")

telegram = telegram_handler(os.environ['secret_telegram_token'], update_from_telegram)
telegram_name = telegram.updater.bot.name
telegram()

for chat_id in get_db().get_all_chats():
    print(chat_id)

debug = s2b(os.environ['DEBUG'], False)

logging.info("starting smtp...")
smtp = smtp_handler(
    os.environ['imap_server'],
    os.environ['imap_user'],
    os.environ['imap_pass'],
    update_from_smtp,
    debug_stop_after_first=debug,
    debug_use_dummy_mails=debug
)

smtp()

# smtp() does idling at the moment
# Idler().idle()


# kill threads
telegram.stop()
sys.exit(0)
