#!/usr/bin/env python3text

from telegram.ext import Updater
from telegram.ext import Handler as BaseHandler

from abstract_update import abstract_update


class MyHandler(BaseHandler):
    def check_update(self, update):
        return True

    def handle_update(self, update, dispatcher, check_result, context=None):
        return self.callback(update, dispatcher.bot)


class Handler(object):

    def handle_telegram_update(self, update, bot):
        this_abstract_update = abstract_update(update.message.text, update.message.chat_id)
        self.callback(this_abstract_update)

    def __init__(self, secret_telegram_token, callback):
        self.callback = callback
        self.updater = Updater(token=secret_telegram_token, use_context=True)
        echo_handler = MyHandler(self.handle_telegram_update)
        self.updater.dispatcher.add_handler(echo_handler)

    def __call__(self):
        self.updater.start_polling()

    def send(self, chat_id, text):
        self.updater.bot.send_message(chat_id=chat_id, text=text)

    def stop(self):
        self.updater.stop()
