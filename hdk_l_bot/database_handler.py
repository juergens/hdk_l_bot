
import sqlite3
import os
import imp
from helper import l
from datetime import datetime

def make_migration(db_file, migration_folder):

    if os.path.isfile(db_file):
        con = sqlite3.connect(db_file)
        cur = con.cursor()
        cur.execute('SELECT name FROM version ORDER BY name DESC LIMIT 1 ')
        last_migration = cur.fetchone()[0]
        cur.close()
    else:
        last_migration = None
        con = sqlite3.connect(db_file)

    skipping_to_current = True
    migrations_were_executed = False

    if last_migration is None:
        skipping_to_current = False
    for mig_file in sorted(os.listdir(migration_folder)):
        if mig_file.startswith("_"):
            continue
        if skipping_to_current:
            if mig_file == last_migration:
                skipping_to_current = False
            continue

        migrations_were_executed = True
        l("applying migration: " + mig_file)
        migration = imp.load_source(mig_file[:-3], os.path.join(migration_folder, mig_file))
        if migration.name != mig_file:
            raise RuntimeError("the name of the migration-file must be equale to the name of the migration")
        try:
            con.execute("begin")
            migration.check_pre_condition(con)
            migration.migrate(con)
            con.execute("commit")
        except Exception as e:
            con.execute('rollback')
            l.e("Error during migrtion!!")
            l.e(e)
            raise e
    if migrations_were_executed:
        l("all migration are executed")
    else:
        l("no migrations were executed")

class Handler(object):

    def __init__(self, db_file):
        self.con = sqlite3.connect(db_file)

    def add_chat_id(self, chat_id):
        cur = self.con.cursor()
        cur.execute("INSERT OR IGNORE INTO telegram_subscription(chat_id, update_at) values (?, ?)", (chat_id,datetime.now()))
        self.con.commit()

    def remove_chat_id(self, chat_id):
        cur = self.con.cursor()
        cur.execute("DELETE FROM telegram_subscription WHERE chat_id = ?", (chat_id,))
        self.con.commit()

    def get_all_chats(self):
        return self.con.cursor().execute('select chat_id from telegram_subscription').fetchall()
