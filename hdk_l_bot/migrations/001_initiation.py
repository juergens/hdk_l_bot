
import sqlite3
from datetime import datetime

class Wrong_Precondition(Exception):
    pass

name = '001_initiation.py'

def check_pre_condition(con):
    cur = con.cursor()
    cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='version';")
    if cur.fetchone() is not None:
        raise Wrong_Precondition()
    cur.close()

def migrate(con):
    cur = con.cursor()
    cur.execute("""
    CREATE TABLE version (
      id integer  NOT NULL PRIMARY KEY,
      name TEXT,
      update_at timestamp 
    );
    """)
    cur.execute("INSERT INTO version(name, update_at) values (?, ?)", (name,datetime.now()))

    cur.close()