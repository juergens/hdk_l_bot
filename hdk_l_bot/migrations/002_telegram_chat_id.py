
import sqlite3
from datetime import datetime

class Wrong_Precondition(Exception):
    pass

name = '002_telegram_chat_id.py'

def check_pre_condition(con):
    cur = con.cursor()
    cur.execute("SELECT name FROM version WHERE name='001_initiation.py';")
    if cur.fetchone() is None:
        raise Wrong_Precondition()
    cur.close()

def migrate(con):
    cur = con.cursor()
    cur.execute("""
    CREATE TABLE telegram_subscription (
      chat_id integer NOT NULL PRIMARY KEY,
      update_at timestamp 
    );
    """)
    cur.execute("INSERT INTO version(name, update_at) values (?, ?)", (name,datetime.now()))

    cur.close()