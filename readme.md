# Hadiko-L Bot für Telegram

A service, that reads Mails from Hadiko-L and forwards them to telegram channels


# Dev-information

- https://www.google.com/settings/security/lesssecureapps
- https://gist.github.com/robulouski/7441883

## relevant links

- https://pypi.org/project/irc/
- https://docs.python.org/3/library/email.examples.html
- https://github.com/LonamiWebs/Telethon
- https://github.com/python-telegram-bot/python-telegram-bot/wiki/Introduction-to-the-API

## testing

test-run locally:

	ln -s docker-compose-dev.yml docker-compose.override.yml
	docker-compose up --build

## setting up with bot-father

todo with botfather when setting up new bot: 

- get token and store it in .env
- setting up commands, so telegram can suggest commands for the user
- note: no privacy mode needed

## CI

if for some reason the CI-Runner stops working, you can set up a new one like this
get token from: Setting-->
CI / CD Settings --> Runners --> Specific Runners --> Setup a specific Runner manually
Configure Runner with token from above:

    docker run --rm -t -i -v /srv/gitlab-runner-hdk_l_bot/config:/etc/gitlab-runner --name gitlab-runner-hdk_l_bot gitlab/gitlab-runner register

(you may have to remove `/srv/gitlab-runner-hdk_l_bot` beforehand)

    sudo nano /srv/gitlab-runner-hdk_l_bot/config/config.toml #privileged = true

start runner

    docker run -d --restart always -v /srv/gitlab-runner-hdk_l_bot/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock --name gitlab-runner-hdk_l_bot gitlab/gitlab-runner:latest

view runner's logs

    docker logs gitlab-runner-stabbot
    
## CD

install `docker-hook`
  

    chmod +x /usr/bin/deploy_hdk_l_bot.sh
    chown wotanii:wotanii /var/log/deploy_hdk_l_bot.log
    
    uuidgen
    crontag -e
    # @reboot /usr/local/bin/docker-hook -t <deploy_token> --port 8556 -c sh /usr/bin/deploy_hdk_l_bot.sh
    sudo reboot now

und dann in gitlab das deploy_token eintragen unter: config-->runner-->variables oderso

das script sieht so aus:

    cat /usr/bin/deploy_hdk_l_bot.sh
    #! /bin/bash
    
    LOG=/var/log/deploy_hdk_l_bot.log
    DC=/usr/local/bin/docker-compose
    date -Iseconds >> $LOG 2>&1
    cd /srv/hdk_l_bot >> $LOG 2>&1
    $DC pull >> $LOG 2>&1
    $DC down >> $LOG 2>&1
    $DC up -d >> $LOG 2>&1
    echo "done" >> $LOG 2>&1